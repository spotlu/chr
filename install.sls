# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "vrsa_dev/update_google_chrome/chrome/map.jinja" import chrome_settings with context %}

{% set OSFAMILY = salt['grains.get']('os_family') %}

check_chrome_installed:
  cmd.run:
    - name: chrome_detector.ps1
    - shell: powershell
    - env:
      - ExecutionPolicy: "bypass"

{% if OSFAMILY == "Windows" %}
create_download_dir:
  file.directory:
    - name: {{ chrome_settings.download.location }}

{% for process in chrome_settings.config.processes %}
pre_stop_process_{{ process }}:
  cmd.run:
    - name: Stop-Process -Name {{ process }} -Force -ErrorAction Ignore
    - shell: powershell
    - onlyif:
      - tasklist /FI "IMAGENAME eq {{ process }}.exe" 2>NUL | find /I /N "{{ process }}.exe">NUL
{% endfor %}

{# NOTE: state to stop the google update services
 # however, by default these are already stopped
 {% for service in chrome_settings.config.services %}
 pre_stop_service_{{ service }}:
   cmd.run:
     - name: Stop-Service -Name {{ service }} -Force -ErrorAction Ignore
     - shell: powershell
{% endfor %}
#}

{% if chrome_settings.source.type == "local" %}
download_local_notification:
  - test.show_notification:
    - text: "Downloading chrome msi from local source"
    - require:
      - create_download_dir

download_local_msi:
  file.managed:
    - name: C:\Downloads\{{ chrome_settings.config.filename }}
    - source: {{ chrome_settings.source.local }}
    - makedirs: true

{% elif chrome_settings.source.type == "remote" %}
download_remote_notification:
  - test.show_notification:
    - text: "Downloading chrome msi from remote source"
    - require:
      - create_download_dir

download_remote_msi:
  file.managed:
    - name: Copy-Item -Path {{ chrome_settings.source.remote }} -Destination "{{ chrome_settings.download.location }}\{{ chrome_settings.config.filename }}" -Force
    - shell: powershell
    - create: {{ chrome_settings.download.location }}\{{ chrome_settings.config.filename }}
    - require:
      - create_download_dir
{% endif %}

update_chrome_notification:
  test.show_notification:
    - test: "Updating chrome to latest version"
    - require:
      - download_{{ chrome_settings.source.type }}_msi

install_chrome_msi:
  cmd.run:
    - name: msiexec /i {{ chrome_settings.download.location }}\{{ chrome_settings.config.filename }}
    - shell: powershell
    - env:
      - ExecutionPolicy: "bypass"
    - require:
      - download_{{ chrome_settings.source.type }}_msi

{% for process in chrome_settings.config.processes %}
{% if process == "GoogleUpdate" %}
post_stop_process_{{ process }}:
  cmd.run:
    - name: Stop-Process -Name {{ process }} -Force -ErrorAction Ignore
    - shell: powershell
    - require:
      - install_chrome_msi
{% endif %}
{% endfor %}

{% for service in chrome_settings.config.services %}
configure_service_{{ service }}:
  cmd.run:
    - name: |
        Stop-Service -Name {{ service }} -Force -ErrorAction Ignore
        Set-Service -Name {{ service }} -StartupType Manual -ErrorAction Ignore
    - shell: powershell
    - require:
      - install_chrome_msi
{% endfor %}

{% else %}
skip_non_windows:
  test.show_notification:
    - text: "Chrome update state is not supported on {{ OSFAMILY }}."
{% endif %}