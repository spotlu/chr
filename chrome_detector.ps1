$chromePath = (get-item -path 'registry::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\chrome.exe' 2> $null)
if ($chromePath){
  if (($chromePath.GetValue('')) -match "chrome.exe") {
      Set-Content -Path "C:\chrome-path.txt" -Value "true" -NoNewline
  } else {
      Set-Content -Path "C:\chrome-path.txt" -Value "false" -NoNewline
  }
} else {
  Set-Content -Path "C:\chrome-path.txt" -Value "false" -NoNewline 
}