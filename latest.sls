# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "vrsa_dev/update_google_chrome/chrome/map.jinja" import chrome_settings with context %}

{% set OSFAMILY = salt['grains.get']('os_family') %}

{% if OSFAMILY == "Windows" %}
update_chrome_latest:
  pkg.installed:
    - name: chrome
    - version: latest
    - refresh: false
{% else %}
skip_non_windows:
  test.show_notification:
    - text: "Chrome update state is not supported on {{ OSFAMILY }}."
{% endif %}