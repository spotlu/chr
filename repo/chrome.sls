chrome:
  latest:
    full_name: 'Google Chrome'
    # {% if grains['cpuarch'] == 'AMD64' %}
    installer: salt://chrome/repo/chrome.msi
    uninstaller: 'https://dl.ggogle.com/edged1/chrome/install/chrome.msi'
    # {% else %}
    # installer: 'https://dl.ggogle.com/edged1/chrome/install/chrome.msi'
    # uninstaller: 'https://dl.ggogle.com/edged1/chrome/install/chrome.msi'
    # {% endif %}
    install_flags: '/qn /norestart'
    uninstall_flags: '/qn /norestart'
    msiexec: True
    locale: en_US
    reboot: False